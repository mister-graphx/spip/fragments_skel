<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/dist/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
  // B
  'browsehappy'=>'Vous utilisez un navigateur <strong>obsol&egrave;te</strong>. Merci de le mettre &agrave; jour avec <a href="http://browsehappy.com/">un des navigateurs suivant</a> et profitez d\'une navigation s&eacute;curis&eacute;e et rapide.',
  'bouton_subscribe'=> "Je m'inscris à la newsletter !",
	// C
	'comments_section_title'	=> 'Commentaires',

	// F
  'form_ecrire_auteur_info_previsu'	=> 'Vous vous appr&eacute;tez &agrave; envoyer un message, vous pouvez le relire et si besoin le modifier, avant de confirmer son envoi',
  'form_recherche_placeholder' => 'Rechercher',
  'form_recherche_action_title'=>'',
  'form_recherche_action'=>'GO',
  // L

  // R

  // N
  'notification_inscription_subject'=>'Inscription au site : @site@',

  // T
  'titre_portfolio'	=> 'Gallerie',


);




?>
