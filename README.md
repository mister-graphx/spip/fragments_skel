# Fragments Skel

Un squelette de la famille Zspip intégrant le minimum syndical (markup et dépendances) pour réaliser un site HTML5/css3, responsive, modulaire, et pouvant servir de base à dupliquer ou à utiliser ainsi.

## Thème et personnalisation

**Ce squelette est livré avec une base css minimaliste, à vous de jouer ! Ou faites une demande pour un thème sur [www.mister-graphx.com](https://www.mister-graphx.com).**
