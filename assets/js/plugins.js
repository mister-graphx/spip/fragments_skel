/*!
 * jQuery.localScroll
 * Copyright (c) 2007 Ariel Flesler - aflesler<a>gmail<d>com | https://github.com/flesler
 * Licensed under MIT
 * http://flesler.blogspot.com/2007/10/jquerylocalscroll-10.html
 * @author Ariel Flesler
 * @version 2.0.0
 */
;(function(plugin) {
	// AMD Support
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], plugin);
	} else {
		plugin(jQuery);
	}
}(function($) {
	var URI = location.href.replace(/#.*/, ''); // local url without hash

	var $localScroll = $.localScroll = function(settings) {
		$('body').localScroll(settings);
	};

	// Many of these defaults, belong to jQuery.ScrollTo, check it's demo for an example of each option.
	// @see http://demos.flesler.com/jquery/scrollTo/
	// The defaults are public and can be overriden.
	$localScroll.defaults = {
		duration: 1000, // How long to animate.
		axis: 'y', // Which of top and left should be modified.
		event: 'click', // On which event to react.
		stop: true, // Avoid queuing animations
		target: window, // What to scroll (selector or element). The whole window by default.
		autoscroll: true // If true, applies the scrolling at initial page load.
		/*
		lock: false, // ignore events if already animating
		lazy: false, // if true, links can be added later, and will still work.
		filter: null, // filter some anchors out of the matched elements.
		hash: false, // if true, the hash of the selected link, will appear on the address bar.
		onBefore: null // called before scrolling, "this" contains the settings and gets 3 arguments
		*/
	};

	$.fn.localScroll = function(settings) {
		settings = $.extend({}, $localScroll.defaults, settings);

		if (settings.autoscroll && settings.hash && location.hash) {
			if (settings.target) window.scrollTo(0, 0);
			scroll(0, location, settings);
		}

		return settings.lazy ?
			// use event delegation, more links can be added later.
			this.on(settings.event, 'a,area', function(e) {
				if (filter.call(this)) {
					scroll(e, this, settings);
				}
			}) :
			// bind concretely, to each matching link
			this.find('a,area')
				.filter(filter).bind(settings.event, function(e) {
					scroll(e, this, settings);
				}).end()
			.end();

		function filter() {// is this a link that points to an anchor and passes a possible filter ? href is checked to avoid a bug in FF.
			return !!this.href && !!this.hash && this.href.replace(this.hash,'') === URI && (!settings.filter || $(this).is(settings.filter));
		}
	};

	// Not needed anymore, kept for backwards compatibility
	$localScroll.hash = function() {};

	function scroll(e, link, settings) {
		var id = link.hash.slice(1),
			elem = document.getElementById(id) || document.getElementsByName(id)[0];

		if (!elem) return;

		if (e) e.preventDefault();

		var $target = $(settings.target);

		if (settings.lock && $target.is(':animated') ||
			settings.onBefore && settings.onBefore(e, elem, $target) === false)
			return;

		if (settings.stop) {
			$target.stop(true); // remove all its animations
		}

		if (settings.hash) {
			var attr = elem.id === id ? 'id' : 'name',
				$a = $('<a> </a>').attr(attr, id).css({
					position:'absolute',
					top: $(window).scrollTop(),
					left: $(window).scrollLeft()
				});

			elem[attr] = '';
			$('body').prepend($a);
			location.hash = link.hash;
			$a.remove();
			elem[attr] = id;
		}

		$target
			.scrollTo(elem, settings) // do scroll
			.trigger('notify.serialScroll',[elem]); // notify serialScroll about this change
	}

	// AMD requirement
	return $localScroll;

}));

/*!
 * jQuery.scrollTo
 * Copyright (c) 2007-2015 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * http://flesler.blogspot.com/2007/10/jqueryscrollto.html
 * @projectDescription Lightweight, cross-browser and highly customizable animated scrolling with jQuery
 * @author Ariel Flesler
 * @version 2.1.2
 */
;(function(factory) {
	'use strict';
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof module !== 'undefined' && module.exports) {
		// CommonJS
		module.exports = factory(require('jquery'));
	} else {
		// Global
		factory(jQuery);
	}
})(function($) {
	'use strict';

	var $scrollTo = $.scrollTo = function(target, duration, settings) {
		return $(window).scrollTo(target, duration, settings);
	};

	$scrollTo.defaults = {
		axis:'xy',
		duration: 0,
		limit:true
	};

	function isWin(elem) {
		return !elem.nodeName ||
			$.inArray(elem.nodeName.toLowerCase(), ['iframe','#document','html','body']) !== -1;
	}		

	$.fn.scrollTo = function(target, duration, settings) {
		if (typeof duration === 'object') {
			settings = duration;
			duration = 0;
		}
		if (typeof settings === 'function') {
			settings = { onAfter:settings };
		}
		if (target === 'max') {
			target = 9e9;
		}

		settings = $.extend({}, $scrollTo.defaults, settings);
		// Speed is still recognized for backwards compatibility
		duration = duration || settings.duration;
		// Make sure the settings are given right
		var queue = settings.queue && settings.axis.length > 1;
		if (queue) {
			// Let's keep the overall duration
			duration /= 2;
		}
		settings.offset = both(settings.offset);
		settings.over = both(settings.over);

		return this.each(function() {
			// Null target yields nothing, just like jQuery does
			if (target === null) return;

			var win = isWin(this),
				elem = win ? this.contentWindow || window : this,
				$elem = $(elem),
				targ = target, 
				attr = {},
				toff;

			switch (typeof targ) {
				// A number will pass the regex
				case 'number':
				case 'string':
					if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
						targ = both(targ);
						// We are done
						break;
					}
					// Relative/Absolute selector
					targ = win ? $(targ) : $(targ, elem);
					/* falls through */
				case 'object':
					if (targ.length === 0) return;
					// DOMElement / jQuery
					if (targ.is || targ.style) {
						// Get the real position of the target
						toff = (targ = $(targ)).offset();
					}
			}

			var offset = $.isFunction(settings.offset) && settings.offset(elem, targ) || settings.offset;

			$.each(settings.axis.split(''), function(i, axis) {
				var Pos	= axis === 'x' ? 'Left' : 'Top',
					pos = Pos.toLowerCase(),
					key = 'scroll' + Pos,
					prev = $elem[key](),
					max = $scrollTo.max(elem, axis);

				if (toff) {// jQuery / DOMElement
					attr[key] = toff[pos] + (win ? 0 : prev - $elem.offset()[pos]);

					// If it's a dom element, reduce the margin
					if (settings.margin) {
						attr[key] -= parseInt(targ.css('margin'+Pos), 10) || 0;
						attr[key] -= parseInt(targ.css('border'+Pos+'Width'), 10) || 0;
					}

					attr[key] += offset[pos] || 0;

					if (settings.over[pos]) {
						// Scroll to a fraction of its width/height
						attr[key] += targ[axis === 'x'?'width':'height']() * settings.over[pos];
					}
				} else {
					var val = targ[pos];
					// Handle percentage values
					attr[key] = val.slice && val.slice(-1) === '%' ?
						parseFloat(val) / 100 * max
						: val;
				}

				// Number or 'number'
				if (settings.limit && /^\d+$/.test(attr[key])) {
					// Check the limits
					attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max);
				}

				// Don't waste time animating, if there's no need.
				if (!i && settings.axis.length > 1) {
					if (prev === attr[key]) {
						// No animation needed
						attr = {};
					} else if (queue) {
						// Intermediate animation
						animate(settings.onAfterFirst);
						// Don't animate this axis again in the next iteration.
						attr = {};
					}
				}
			});

			animate(settings.onAfter);

			function animate(callback) {
				var opts = $.extend({}, settings, {
					// The queue setting conflicts with animate()
					// Force it to always be true
					queue: true,
					duration: duration,
					complete: callback && function() {
						callback.call(elem, targ, settings);
					}
				});
				$elem.animate(attr, opts);
			}
		});
	};

	// Max scrolling position, works on quirks mode
	// It only fails (not too badly) on IE, quirks mode.
	$scrollTo.max = function(elem, axis) {
		var Dim = axis === 'x' ? 'Width' : 'Height',
			scroll = 'scroll'+Dim;

		if (!isWin(elem))
			return elem[scroll] - $(elem)[Dim.toLowerCase()]();

		var size = 'client' + Dim,
			doc = elem.ownerDocument || elem.document,
			html = doc.documentElement,
			body = doc.body;

		return Math.max(html[scroll], body[scroll]) - Math.min(html[size], body[size]);
	};

	function both(val) {
		return $.isFunction(val) || $.isPlainObject(val) ? val : { top:val, left:val };
	}

	// Add special hooks so that window scroll properties can be animated
	$.Tween.propHooks.scrollLeft = 
	$.Tween.propHooks.scrollTop = {
		get: function(t) {
			return $(t.elem)[t.prop]();
		},
		set: function(t) {
			var curr = this.get(t);
			// If interrupt is true and user scrolled, stop animating
			if (t.options.interrupt && t._last && t._last !== curr) {
				return $(t.elem).stop();
			}
			var next = Math.round(t.now);
			// Don't waste CPU
			// Browsers don't render floating point scroll
			if (curr !== next) {
				$(t.elem)[t.prop](next);
				t._last = this.get(t);
			}
		}
	};

	// AMD requirement
	return $scrollTo;
});

/**


Mieux détecter les changement de viewport et surtout l'orientation
En utilisant matchMedia : https://developer.mozilla.org/en-US/docs/Web/API/Window/matchMedia
plutot qu'une détection de la largeur de l'écran.

https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Testing_media_queries


@function
@version
@name responsiveMenu
@param format multitoggle megaDropDown
@param sticky
@param breakpoint
@example:
(function($){
    $(document).ready(function(){
        $(".js-menumaker").responsiveMenu({
           format: "multitoggle",
           sticky:false,
           stickyClass: 'sticky',
           stickyTreshold: 50,
           breakpoint: 780,
           ulOpenClass:'open',
           onClickOutsideClose:true
        });
        var headDesktopHeight = $('.head-desktop').height();
        var menu = $('.js-menumaker').responsiveMenu({
            format: 'megaDropDown',
            sticky: false,
            stickyTreshold: headDesktopHeight
        });
        menu.on();
    });
})(jQuery);


@see https://github.com/dollarshaveclub/stickybits
@todo - enlever la dependance a jquery
@todo - aria sur le bouton mobile
@todo - priority+ pattern https://codepen.io/Dreamdealer/pen/waVzmK


*/
(function($) {

  $.fn.responsiveMenu = function(user_options) {
      'use strict';

      if ( typeof(user_options) == "undefined" || user_options === null ) {
          user_options = {};
      }

      var RM = {

        options: $.extend({
          format: "multitoggle",
          sticky: false,
          stickyClass: 'sticky',
          stickyTreshold: 50,
          breakpoint: 769,
          landscapeMaxWidth: 1025,
          ulOpenClass:'open',
          onClickOutsideClose:true
        }, user_options),
        //
        isOpen: false,
        mainContainer: $(this),
        menuContainer: $(this).find('.menu-container'),
        mainMenu: $(this).find('.menu-container > ul'),
        mobileButton: $(this).find(".mobile-button"),
        windowWidth: $(window).width(),
        // Util :
        closeMenu: function (){
          RM.mainMenu.removeClass('is-open');
          RM.mainMenu.removeClass('overlay');
          RM.mobileButton.removeClass('menu-opened');
          RM.menuContainer.removeClass('is-open');
          RM.mainMenu.find('.has-sub').removeClass("open");
        },

        collapseAll: function (){
          RM.mainMenu.find('li.has-sub .submenu-button.submenu-opened').removeClass('submenu-opened');
          RM.mainMenu.find('li.has-sub ul.' + RM.options.ulOpenClass).removeClass(RM.options.ulOpenClass);
        },

        // @todo une fonction utile pour placer des sous-menu dans le viewport
        // http://jsfiddle.net/G7qfq/582/
        inView: function (){
          $(".megadropdown li").on('mouseenter mouseleave', function (e) {
               if ($('ul', this).length) {
                   var elm = $('ul:first', this);
                   var off = elm.offset();
                   var l = off.left;
                   var w = elm.width();
                   var docH = $(".container").height();
                   var docW = $(".container").width();

                   var isEntirelyVisible = (l + w <= docW);

                   if (!isEntirelyVisible) {
                       $(this).addClass('edge');
                   } else {
                       $(this).removeClass('edge');
                   }
               }
          });
        },
        // Mode multiToggle
        // Une vue large navigation avec sous-menus arborescents
        //
        multiToggle: function (){
          // Ajouter les bouton d'état
          RM.mainMenu.find(".has-sub > a").append('<span class="submenu-button"></span>');
          // Positionner correctement les sous-menus
          // En mode large uniquement si les sous-menus s'affiche soit
          // a droite/soit a gauche suivant leur position dans le viewport
          if (RM.windowWidth > RM.options.breakpoint) {
            RM.mainMenu.find("> li.has-sub > a").on('mouseenter',function(e){
                var ParentElem = $(this).parent();
                var level1 = ParentElem.find('> ul');
                var subMenusWidth = level1.width();
                // 1er niveau
                if(typeof(level1) != undefined){
                    // console.log('HasSub     Offset left : ' + level1.offset().left);
                    // console.log('HasSub Witdh : ' + level1.width());
                    // console.log('Window Width : ' + RM.windowWidth );

                    // pour les sous menu les plus proches de la droite de l'écran
                    // on positionne le sous-menu a doite de l'item
                  if( (level1.offset().left + subMenusWidth ) > RM.windowWidth) {
                      level1.css('right', 0);
                  }
                  // Au survol

                  // submenus lists
                  var sub = level1.find('.has-sub > ul');
                  // console.log(sub[0]);
                  if(typeof(sub) != 'undefined' && sub.length > 0){
                      sub.each(function(i){
                          // console.log($(this).offset().left);
                        var subOffset = level1.offset().left + (subMenusWidth * 2) ;
                        if ( subOffset > RM.windowWidth ){
                          $(this).css('margin-left', '-100%');
                        }
                      });
                  }
                }
            });
          }
          // Evenement click sur une liste ayant des enfants
          RM.mainMenu.find("li.has-sub > a").on('click',function(e){
            // En vue mobile/collapse on déplie les menus
            if (RM.windowWidth < RM.options.breakpoint) {
                e.preventDefault();

                var ParentElem = $(this).parent();
                var icnState = $(this).find(".submenu-button");
                
                icnState.toggleClass('submenu-opened');
                ParentElem.find('> ul').toggleClass(RM.options.ulOpenClass);
            }
            // En vue large, le clic sur une item ferme les autres menus eventuellements ouverts
            if (RM.windowWidth > RM.options.breakpoint) {
                    //console.log('click');
              // var others = RM.mainMenu.find('li.has-sub').not(ParentElem);
              // others.each(function(){
              //   $(this).find('.submenu-button.submenu-opened').removeClass('submenu-opened');
              //   $(this).find('ul.' + RM.options.ulOpenClass).removeClass(RM.options.ulOpenClass);
              // });
            }
          });
        },
        // Mode megaDropDown
        // En vue large auto centre les mega-dropdown menu dans le viewport
        megaDropDown: function (){
            // en mode megaDropDown on parcours juste le premier niveau de listes
            // Ajoute le bouton d'état deplie/replie
            RM.mainMenu.find(".has-sub > a").append('<span class="submenu-button"></span>');
            // Evenement Clic sur les liens de premier niveau
            RM.mainMenu.find("> li.has-sub > a").on('click',function(e){
                window.scrollTo(0,0);
                RM.mainMenu.toggleClass('overlay');

                var ParentElem = $(this).parent().toggleClass(RM.options.ulOpenClass);
                var icnState = ParentElem.find(".submenu-button").toggleClass('submenu-opened');

              // En vue large
              if (RM.windowWidth > RM.options.breakpoint) {
                // le clic sur une item ferme les autres menus eventuellements ouverts
                var others = RM.mainMenu.find('li.has-sub').not(ParentElem);
                others.each(function(){
                  $(this).find('.submenu-button.submenu-opened').removeClass('submenu-opened');
                  $(this).find('ul.' + RM.options.ulOpenClass).removeClass(RM.options.ulOpenClass);
                });

              }
            //
            // icnState.toggleClass('submenu-opened');
            // ParentElem.find('ul').toggleClass(RM.options.ulOpenClass);
            e.preventDefault();
          });
        },
        megaDropDownReload: function (){
            if (RM.windowWidth > RM.options.breakpoint) {
                // verifier que le sous-menu est bien dans le viewport
                var megaDropDownMenu = RM.mainMenu.find("> .has-sub > ul");
                var parent = megaDropDownMenu.parent();

                var parentOffsetleft = parent[0].offsetLeft;
                var megaDropDownW = megaDropDownMenu.outerWidth();

                var docW = RM.windowWidth - 20;

                if(RM.windowWidth < 1200 ){
                  var setWidth = megaDropDownMenu.css({
                    maxWidth: docW,
                    //position:'absolute',
                    left: -( parentOffsetleft - 10 ),
                  });
                }
            }
        },
        dropDown: function (){
            // en mode dropDown on parcours juste le premier niveau de listes
            // Ajoute le bouton d'état deplie/replie
            // Sur le premier niveau
            RM.mainMenu.find(".has-sub > a").append('<span class="submenu-button"></span>');
            // Evenement Clic sur les liens de premier niveau
            RM.mainMenu.find("> li.has-sub > a").on('click',function(e){
                if (RM.windowWidth < RM.options.breakpoint) {
                    e.preventDefault();

                    // Se positionner en haut de page
                    window.scrollTo(0,0);
                    RM.mainMenu.toggleClass('overlay');

                    var ParentElem = $(this).parent().toggleClass(RM.options.ulOpenClass);
                    var icnState = ParentElem.find(".submenu-button").toggleClass('submenu-opened');
                }


              // En vue large
              if (RM.windowWidth > RM.options.breakpoint) {
                // // le clic sur une item ferme les autres menus eventuellements ouverts
                // var others = RM.mainMenu.find('li.has-sub').not(ParentElem);
                // others.each(function(){
                //   $(this).find('.submenu-button.submenu-opened').removeClass('submenu-opened');
                //   $(this).find('ul.' + RM.options.ulOpenClass).removeClass(RM.options.ulOpenClass);
                // });

              }
            //
            // icnState.toggleClass('submenu-opened');
            // ParentElem.find('ul').toggleClass(RM.options.ulOpenClass);
            // e.preventDefault();
          });
        },

        resizeFix: function () {
            // Update windowWidth
            RM.windowWidth = $(window).width();
            RM.collapseAll();
            RM.closeMenu();
            if(RM.options.format === 'megadropdown'){
              RM.megaDropDownReload();
            }
        },

        addAriaMarkup: function(){
          // Add ARIA role to menubar and menu items
          RM.mainmenu.attr('role', 'menubar').attr('aria-hidden', false).find('li').attr('role', 'menuitem');
          var first_level_links = RM.mainmenu.find('> li > a');
          //
          // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
          $(first_level_links).next('ul')
              .attr({ 'aria-hidden': 'true', 'role': 'menu' })
              .find('a')
              .attr('tabIndex',-1);

          // Adding aria-haspopup for appropriate items
          $(first_level_links).each(function(){
            if($(this).next('ul').length > 0)
              $(this).parent('li').attr('aria-haspopup', 'true');
          });

          $(first_level_links).on('hover',function(){
              $(this).closest('ul')
                .find('.' + RM.options.ulOpenClass)
                .attr('aria-hidden', 'true')
                .removeClass(RM.options.ulOpenClass)
                .find('a')
                .attr('tabIndex',-1);
              $(this).next('ul')
                .attr('aria-hidden', 'false')
                .addClass(RM.options.ulOpenClass)
                .find('a').attr('tabIndex',0);
          });

          $(top_level_links).on('focus',function(){
              $(this).closest('ul')
                .find('.' + RM.options.ulOpenClass)
                .attr('aria-hidden', 'true')
                .removeClass(RM.options.ulOpenClass)
                .find('a')
                .attr('tabIndex',-1);

              $(this).next('ul')
                .attr('aria-hidden', 'false')
                .addClass(RM.options.ulOpenClass)
                .find('a').attr('tabIndex',0);
          });

          $(document).on('click touchstart',function(){
              $('.'+settings.cssMenuOpen).attr('aria-hidden', 'true')
                    .removeClass(RM.options.ulOpenClass)
                    .find('a')
                    .attr('tabIndex',-1);
          });
        },

        initMenu: function (){
          //addAriaMarkup();
          RM.windowWidth = $(window).width();
          RM.menuContainer.find('li ul').parent().addClass('has-sub');
          RM.initiateClickListeners();
          RM.initiateResizeListener();

          if (RM.options.format === 'multitoggle'){
              RM.mainContainer.addClass('multitoggle');
              RM.multiToggle();
          }
          else if (RM.options.format === 'megadropdown'){
              RM.mainContainer.addClass('megadropdown');
              RM.megaDropDown();
              RM.megaDropDownReload();
          }
          else if (RM.options.format === 'dropdown'){
              RM.mainContainer.addClass('dropdown');
              RM.dropDown();
          }
          else {
              console.log('Type de menu inconnu :' + RM.options.format);
          }
          if (RM.options.sticky === true){
              $(window).scroll(function() {
                  var stickyClass = RM.options.stickyClass;
                  var treshold = RM.options.stickyTreshold;
                  if( $(this).scrollTop() > treshold) {
                    RM.mainContainer.addClass(stickyClass);
                  } else {
                    RM.mainContainer.removeClass(stickyClass);
                  }
              });
          }
        },

        initiateClickListeners: function (){
          RM.mobileButton.on('click', function(e){
              if(RM.isOpen == false) {
                window.scrollTo(0,0);
                $(this).addClass('menu-opened');
                RM.menuContainer.addClass('is-open');
                RM.mainMenu.addClass('is-open');
                RM.isOpen= true;
              }else{
                RM.closeMenu();
                RM.collapseAll();
                RM.isOpen = false;
              }
              e.preventDefault();
          });
          // Fermer le menu au clic en dehors
          // si en mode mobile
          if (RM.options.onClickOutsideClose === true
             && RM.windowWidth < RM.options.breakpoint){
              RM.menuContainer.on('click', function(e){
                 if (RM.menuContainer.hasClass('is-open')
                    && e.target.nodeName === 'NAV'){
                      RM.closeMenu();
                      RM.collapseAll();
                      RM.isOpen = false;
                      e.preventDefault();
                  }
              });
          }
        },

        initiateResizeListener: function (){
          $(window).on('load resize', function(){
            RM.resizeFix();
          });
        }

      };

      return {
        on: function(){
          RM.initMenu();
          RM.resizeFix();
          return this;
        },
        debug: function (){
          console.log(RM.options.format);
        }
      };

  };

})(jQuery);
