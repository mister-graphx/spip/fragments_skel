/**
@file Main
@desc main javascript file
*/


// # Dom Ready
jQuery(function($){
    // Initialize menu
    var menu = $('.js-menumaker').responsiveMenu({
        format: 'multitoggle',
        onClickOutsideClose:true,
    }).on();
	// Local Scroll on Anchor Links
	$.localScroll();
});
