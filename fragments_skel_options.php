<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

// Warning : Maximum function nesting level of '100'
// Principalement pour l'export de configuration avec ieconfig
ini_set('xdebug.max_nesting_level', 200);

// Evite une alerte de validation HTML5
// Sinon passer par le htaccess
header('X-UA-Compatible: IE=edge');

// SECURITE
// globale filtrer_javascript :
// https://core.spip.net/projects/spip/repository/entry/spip/ecrire/inc/texte.php#L142
// -1 : protection dans l'espace privé et public
// 0  : protection dans l'espace public
// 1  : aucune protection
// $GLOBALS['filtrer_javascript'] = 1;

// HTTP Header :
// http://www.spip.net/fr_article4648.html
// $spip_header_silencieux = 1;

// CACHE global du site par defaut : ici 24 x 3600 sec x 30 soit tous les mois
// define('_DUREE_CACHE_DEFAUT', 24*3600*30);
// Débug du cache : ne pas crypter le CACHE
// https://contrib.spip.net/XRay-un-explorateur-de-cache
// define('_CACHE_KEY', '');

// désactiver les cache de spip pendant le dev
// doc : http://programmer.spip.org/Configurer-le-cache
// options : -1 (ne  jamais utiliser),1 (ne pas utiliser, mais stocker), 0 (utiliser si, ou calculer)
// define('_NO_CACHE', -1);
// define('_LOG_FILTRE_GRAVITE', 8);

//plusieurs super-admin (webmesttres)
//define ('_ID_WEBMESTRES','0:57');

// Medias et documents
// https://www.spip.net/fr_article4645.html#_IMG_MAX_WIDTH
// define('_IMG_MAX_WIDTH',1200);
// define('_IMG_MAX_HEIGHT',1200);
// https://www.spip.net/fr_article4644.html#_IMG_MAX_SIZE
// define('_IMG_MAX_SIZE', 350);
// http://www.spip.net/fr_article5524.html#_IMG_GD_QUALITE
// define('_IMG_GD_QUALITE', 80);

// Permet de forcer l'ajout des documents directement dans le porfolio
// http://www.spip.net/fr_article5626.html#_LARGEUR_MODE_IMAGE
define('_LARGEUR_MODE_IMAGE', 500); // Toutes les images de plus de 500px sont ajoutéees au porfolio

// Titrer les documents joints à partir du nom du fichier
// https://www.spip.net/fr_article5674.html
// define('_TITRER_DOCUMENTS', true );

// Autobr
// http://www.spip.net/fr_article5617.html
//define('_AUTOBR', '');

// La balise #INTRODUCTION
// http://www.spip.net/fr_article3965.html
define('_INTRODUCTION_SUITE', '');

// http://www.spip.net/fr_article1825.html#debut_intertitre
// $GLOBALS[`class_spip`] = ' class="spip"';
$GLOBALS['class_spip'] = '';
$GLOBALS['class_spip_plus'] = '';

$GLOBALS['debut_italique'] = '<i>' ;
$GLOBALS['fin_italique'] = '</i>'  ;

$GLOBALS['debut_gras'] = '<strong>' ;
$GLOBALS['fin_gras'] = '</strong>' ;

$GLOBALS['ligne_horizontale'] = '<hr/>';

// Niveau des intertitres
$GLOBALS['debut_intertitre'] = "\n<h2 class=\"h2\">\n";
$GLOBALS['fin_intertitre'] = "</h2>\n";

// ne jamais afficher les préfixes numériques des titres
// $table_des_traitements['TITRE'][]= 'typo(supprimer_numero(%s))';

// http://www.spip.net/fr_article5652.html
// ne pas transformer en lien cliquable les urls écrites dans le corps des textes
// define('_EXTRAIRE_LIENS', '//');
define('_MARKDOWN_PRESERVE_AUTOLIENS', true);

// Activer HTML5 depuis le squelette
$GLOBALS['meta']['version_html_max'] = 'html5';

// Définir la google api key pour webfont, videos
// define('_GOOGLE_API_KEY', '');

// ID de suivi analitic
// insséré par #GA_TRACKING juste après le meta.charset dans structure.html
// define('_GA_UID', 'UA-3310709-2');

// Plugin Z-core
// Definir les blocs Z
if (!isset($GLOBALS['z_blocs']))
	$GLOBALS['z_blocs'] = array('content','head_meta','aside','extra');
// https://contrib.spip.net/Zpip-blocs-de-page-et-Ajax
//define('_Z_AJAX_PARALLEL_LOAD','content');
$GLOBALS['z_blocs_404'] = array('content','aside');

define('_Z_DOCTYPE','<!doctype html>');
// Définir un sprite svg
// https://zone.spip.org/trac/spip-zone/changeset/114892/spip-zone
// define('_ICON_SPRITE_SVG_FILE', "css/bytesize/bytesize-symbols.min.svg");

// Plugin zenGarden
// Référencer les thèmes
define('_ZENGARDEN_FILTRE_THEMES','fragments_skel');

// Plugin Album
define('_ALBUMS_INSERT_HEAD_CSS',false);

// # Autorisations Demo
// Tiré de Spip-r : mettre en place des docs pour les seuls admins
if (defined('_FRAGMENTS_SKEL_AUTH_DEMO')?
		_FRAGMENTS_SKEL_AUTH_DEMO
		:
		(isset($GLOBALS['visiteur_session']['statut'])
    AND $GLOBALS['visiteur_session']['statut']=='0minirezo'
    AND $GLOBALS['visiteur_session']['webmestre']=='oui')
	)
	_chemin(_DIR_PLUGIN_FRAGMENTS_SKEL."demo/");
