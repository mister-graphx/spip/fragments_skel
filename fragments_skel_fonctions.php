<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * pipeline menus_utiles()
 *
 * ajoute des menus par défaut installables au plugins menu
 * 
 * @return array $menus
*/
function fragments_skel_menus_utiles($menus) {
    $menus['nav_primary'] = 'Menu principal';
    $menus['nav_access'] = 'Accès rapide accéssibilité';
    $menus['site_socials_links'] = 'Suivez nous';
	$menus['footer_corporate'] = 'Footer - Corporate';
    $menus['footer_colophon'] = 'Footer - Colophon';
	
	
    return $menus;
}


/**
function filtre_pagination

Surcharge de la balise du core /ecrire/inc/filtres.php
fonction standard de calcul de la balise #PAGINATION
on peut la surcharger en definissant filtre_pagination dans mes_fonctions

ou directement en n'utilisant pas la balise ANCRE mais un lien direct dans le markup

@url : http://doc.spip.org/@filtre_pagination_dist

*/
function filtre_pagination($total, $nom, $position, $pas, $liste = true, $modele='', $connect='', $env=array()) {
	static $ancres = array();
	if ($pas<1) return '';
	$ancre = 'pagination'.$nom; // #pagination_articles
	$debut = 'debut'.$nom; // 'debut_articles'

	// n'afficher l'ancre qu'une fois
	if (!isset($ancres[$ancre]))
		$bloc_ancre = $ancres[$ancre] = "<a id='".$ancre."'></a>";
	else $bloc_ancre = '';
	// liste = false : on ne veut que l'ancre
	if (!$liste)
		return $ancres[$ancre];

	$pagination = array(
		'debut' => $debut,
		'url' => parametre_url(self(),'fragment',''), // nettoyer l'id ahah eventuel
		'total' => $total,
		'position' => intval($position),
		'pas' => $pas,
		'nombre_pages' => floor(($total-1)/$pas)+1,
		'page_courante' => floor(intval($position)/$pas)+1,
		'ancre' => $ancre,
		'bloc_ancre' => $bloc_ancre
	);
	if (is_array($env))
		$pagination = array_merge($env,$pagination);

	// Pas de pagination
	if ($pagination['nombre_pages']<=1)
		return '';

	if ($modele) $modele = '_'.$modele;

	return recuperer_fond("modeles/pagination$modele", $pagination, array('trim'=>true), $connect);
}





?>