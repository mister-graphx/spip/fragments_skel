# CHANGELOG

1.2.0

- classement des inclures

1.1.6

- report de correctifs non publiés lors de la migration, svn > git b3183eba...2a6d70a2

- ajout des fichiers readme et changelogs

1.1.5

maj avant migration svn > git
